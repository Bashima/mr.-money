package com.example.mr.money;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.R.string;
import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.inputmethodservice.*;

public class Budget extends Activity {

	private DrawerLayout drawerLayout;
	private ListView drawable_listview;
	private ActionBarDrawerToggle actionBarDrawerToggle;
	private String filename = "myfile";
	private FileOutputStream outputStream;
	private String month_name;
	
	
	private String readBudgetFile()
	{
		String ret = "";
        
        try {
            InputStream inputStream = openFileInput(filename);
             
            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                 
                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }
                 
                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
            catch (FileNotFoundException e) {
              System.out.println("file not found while reading");
            } catch (IOException e) {
            	
            	System.out.println("i/o exception while reading");
            }
        return ret;
		
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_budget);
		final Dialog dialog = new Dialog(this);
		final Button dButton = (Button) findViewById(R.id.button_budget);
		
	//setting button text oncreate
		dButton.setText(readBudgetFile()+"$");
	
	
		
		//setting current month	
		Calendar cal=Calendar.getInstance();
		SimpleDateFormat month_date = new SimpleDateFormat("MMMMMMMMM");
		month_name = month_date.format(cal.getTime());
		final TextView month=(TextView) findViewById(R.id.budget_month);
		month.setText(month_name);
		
		dButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {      	
	    		dialog.setContentView(R.layout.enter_money_layout);
	    		dialog.setTitle("Budget");
	    		dialog.setCancelable(true);
	    		
	    		Button dialogButton = (Button) dialog.findViewById(R.id.ok_button_dialog);
	    		
				// if button is clicked, close the custom dialog
				dialogButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						EditText amount_edittext= (EditText) dialog.findViewById(R.id.Amount);
						String amount= amount_edittext.getText().toString();
						dButton.setText(String.valueOf(amount)+"$");
						
			////writing budget in the file
						try {
			    			  outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
			    			  outputStream.write(amount.getBytes());
			    			  outputStream.close();
			    			} catch (Exception e) {
			    			  e.printStackTrace();
			    			}
						//DATABSE E DHUKABA AMOUNT K
						dialog.dismiss();
					}
				});
				
				
				////////////////////
				Button cdialogButton = (Button) dialog.findViewById(R.id.cancel_button_dialog);
				// if button is clicked, close the custom dialog
				cdialogButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						
						dialog.dismiss();
					}
				});
				dialog.show();
			}
		});
		//drawer
				Model.LoadModel();
				drawable_listview=(ListView)findViewById(R.id.left_drawer);
				String[] ids=new String[Model.Menu_Items.size()];
				for(int i=0;i<ids.length;i++)
				{
					ids[i]=Integer.toString(i+1);
				}
				ItemAdapter adapter= new ItemAdapter(this, R.layout.row, ids);
				drawable_listview.setAdapter(adapter);
				
				drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
				actionBarDrawerToggle = new ActionBarDrawerToggle(
		                this,                  /* host Activity */
		                drawerLayout,         /* DrawerLayout object */
		                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
		                0,  /* "open drawer" description */
		                0  /* "close drawer" description */
		                );
				drawerLayout.setDrawerListener(actionBarDrawerToggle);
				if (android.os.Build.VERSION.SDK_INT >= 11)
				    getActionBar().setDisplayHomeAsUpEnabled(true);
				drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
				drawable_listview.setOnItemClickListener(new DrawerItemClickListener());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.budget, menu);
		return true;
	}

	public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
 
         // call ActionBarDrawerToggle.onOptionsItemSelected(), if it returns true
        // then it has handled the application icon touch event
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        else
        	return false;
	}
	protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
         actionBarDrawerToggle.syncState();
    }
	
	public class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        	
        	if(position==0)
        	{Intent i = new Intent(Budget.this, Overview.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==1)
            	{Intent i = new Intent(Budget.this, History.class); startActivity(i);
            
            	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==2)
        	{Intent i = new Intent(Budget.this, Catagory.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==3)
        	{Intent i = new Intent(Budget.this, Budget.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==4)
        	{Intent i = new Intent(Budget.this, LoanLend.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==5)
        	{Intent i = new Intent(Budget.this, Savings.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==6)
        	{Intent intent = new Intent(Budget.this,PrefsActivity.class);startActivity(intent);
        	
        	drawerLayout.closeDrawer(drawable_listview);}
        
        }
	}
    
}

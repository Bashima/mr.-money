package com.example.mr.money;

public class Menu_Item {
	
	public int Id;
	public String IconFile;
	public String Name;
	
	public Menu_Item(int id,String iconfile,String name)
	{
		Id=id;
		IconFile=iconfile;
		Name=name;
	}

}

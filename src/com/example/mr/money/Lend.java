package com.example.mr.money;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;


public class Lend extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lend_layout);
        
        ListView LoanLendListView;
       
        Lend_Model.Load_Lend_Model();
        LoanLendListView = (ListView) findViewById(R.id.Lend_List);
        String[] loan_lend_list_ids = new String[Lend_Model.loan_lend_items.size()];
        for (int j= 0; j < loan_lend_list_ids.length; j++){

           loan_lend_list_ids[j] = Integer.toString(j+1);
        }

        Loan_Adapter loan_lend_list_adapter = new Loan_Adapter(this,R.layout.loan_lend_row_layout, loan_lend_list_ids);
        LoanLendListView.setAdapter(loan_lend_list_adapter);
        
        final Button add_lend_Button = (Button) findViewById(R.id.add_lend_button);
		add_lend_Button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialog(1);
				
			}
		});
    }
    @Override
    protected Dialog onCreateDialog(int id) {
    	final Dialog add_lend_dialog = new Dialog(this);
		add_lend_dialog.setContentView(R.layout.lend_add_layout);
		add_lend_dialog.setTitle("Add Lend");
		add_lend_dialog.setCancelable(true);
		Button okdialogButton = (Button) add_lend_dialog.findViewById(R.id.ok_button_dialog);
		// if button is clicked, close the custom dialog
		okdialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				add_lend_dialog.dismiss();

			}
		});
		Button canceldialogButton = (Button) add_lend_dialog.findViewById(R.id.cancel_button_dialog);
		// if button is clicked, close the custom dialog
		canceldialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				add_lend_dialog.dismiss();
			}
		});
		return add_lend_dialog;
 
    }
}
package com.example.mr.money;

public class Category {
	//properties
		 String categoryName;
		 int categoryId;
		 
		 
		 //constructors
		 public Category()
		 {
			 
		 }
		 
		 public Category(String name)
		 {
			 this.categoryName=name;
			 
		 }
		 
		 public Category(int id,String name)
		 {
			 this.categoryId=id;
			 this.categoryName=name;
			 
		 }
		 
		 
		 //setters
		 public void setCategoryName(String name)
		 {
			 System.out.println("here name settingg");
			 this.categoryName=name;
		 }
		 
		 public void setCategoryId(int id)
		 {
			 this.categoryId=id;
		 }
		 
		 
		 //getters
		 public int getCategoryId()
		 {
			 return this.categoryId;
		 }
		 
		 public String getCategoryName()
		 {
			 return this.categoryName;
			 
		 }


}

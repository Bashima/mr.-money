package com.example.mr.money;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class Savings extends Activity {

	private DrawerLayout drawerLayout;
	private ListView drawable_listview;
	private ActionBarDrawerToggle actionBarDrawerToggle;
	Double savings_amount;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_savings);
		
		savings_amount=312.43;
		final Button SavingsAmount= (Button) findViewById(R.id.savings);
		SavingsAmount.setText(savings_amount.toString()+"$");
		
		final Dialog dialog = new Dialog(this);
		
		SavingsAmount.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {      	
	    		dialog.setContentView(R.layout.edit_savings_layout);
	    		dialog.setTitle("Edit Savings");
	    		dialog.setCancelable(true);
	    		Button dialogButton = (Button) dialog.findViewById(R.id.ok_button_dialog);
				// if button is clicked, close the custom dialog
				dialogButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						EditText amount_edittext= (EditText) dialog.findViewById(R.id.Savings_Edit_Amount);
						String prev_amount= (String) SavingsAmount.getText();
						String amount= amount_edittext.getText().toString();
			    		double result;
			    		RadioGroup radioGroup = (RadioGroup) findViewById(R.id.Do_radioGroup);
			    		   int selectedId = radioGroup.getCheckedRadioButtonId();
			    		   RadioButton osButton = (RadioButton) findViewById(selectedId);
			    		if(osButton.getText().equals("Add"))
			    		  result= ((Double.parseDouble(prev_amount.substring(0,prev_amount.length()-1)))+(Double.parseDouble(amount)));
			    		else if (osButton.getText().equals("Deduct")) 
			    			result= ((Double.parseDouble(prev_amount.substring(0,prev_amount.length()-1)))-(Double.parseDouble(amount)));
			    		else
			    			result= (Double.parseDouble(prev_amount.substring(0,prev_amount.length()-1)));
			    			
				        SavingsAmount.setText(String.valueOf(result)+"$");
						dialog.dismiss();
					}
				});
				Button cdialogButton = (Button) dialog.findViewById(R.id.cancel_button_dialog);
				// if button is clicked, close the custom dialog
				cdialogButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						
						dialog.dismiss();
					}
				});
				dialog.show();
			}
		});
		
		//drawer
				Model.LoadModel();
				drawable_listview=(ListView)findViewById(R.id.left_drawer);
				String[] ids=new String[Model.Menu_Items.size()];
				for(int i=0;i<ids.length;i++)
				{
					ids[i]=Integer.toString(i+1);
				}
				ItemAdapter adapter= new ItemAdapter(this, R.layout.row, ids);
				drawable_listview.setAdapter(adapter);
				
				drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
				actionBarDrawerToggle = new ActionBarDrawerToggle(
		                this,                  /* host Activity */
		                drawerLayout,         /* DrawerLayout object */
		                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
		                0,  /* "open drawer" description */
		                0  /* "close drawer" description */
		                );
				drawerLayout.setDrawerListener(actionBarDrawerToggle);
				if (android.os.Build.VERSION.SDK_INT >= 11)
				    getActionBar().setDisplayHomeAsUpEnabled(true);
				drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
				drawable_listview.setOnItemClickListener(new DrawerItemClickListener());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.savings, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

	public boolean onOptionsItemSelected(MenuItem item) {
       if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
           return true;
       }
       else 
    	   return false;
	}
	
	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
         actionBarDrawerToggle.syncState();
    }
	
	public class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        	if(position==0)
        	{Intent i = new Intent(Savings.this, Overview.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==1)
            	{Intent i = new Intent(Savings.this, History.class); startActivity(i);
            
            	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==2)
        	{Intent i = new Intent(Savings.this, Catagory.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==3)
        	{Intent i = new Intent(Savings.this, Budget.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==4)
        	{Intent i = new Intent(Savings.this, LoanLend.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==5)
        	{Intent i = new Intent(Savings.this, Savings.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==6)
        	{Intent intent = new Intent(Savings.this,PrefsActivity.class);startActivity(intent);
        	
        	drawerLayout.closeDrawer(drawable_listview);}

        }
    }
}

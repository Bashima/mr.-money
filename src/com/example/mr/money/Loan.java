package com.example.mr.money;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;


public class Loan extends Activity {
	
	Context context=this;
	ListView LoanLendListView;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loan_layout);       
       
        Loan_Model.Load_Loan_Lend_Model();
        LoanLendListView = (ListView) findViewById(R.id.Loan_List);
        String[] loan_lend_list_ids = new String[Loan_Model.loan_lend_items.size()];
        for (int j= 0; j < loan_lend_list_ids.length; j++){

           loan_lend_list_ids[j] = Integer.toString(j+1);
        }

        Loan_Adapter loan_lend_list_adapter = new Loan_Adapter(this,R.layout.loan_lend_row_layout, loan_lend_list_ids);
        LoanLendListView.setAdapter(loan_lend_list_adapter);
        
        final Button add_loan_Button = (Button) findViewById(R.id.add_loan_button);
    		add_loan_Button.setOnClickListener(new OnClickListener() {
    			@Override
    			public void onClick(View v) {
    				showDialog(1);
    				
    			}
    		});
    }
    
    @Override
    protected Dialog onCreateDialog(int id) {
    	final Dialog add_loan_dialog = new Dialog(this);
		add_loan_dialog.setContentView(R.layout.loan_add_layout);
		add_loan_dialog.setTitle("Add Loan");
		add_loan_dialog.setCancelable(true);
		Button okdialogButton = (Button) add_loan_dialog.findViewById(R.id.ok_button_dialog);
		// if button is clicked, close the custom dialog
		okdialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				add_loan_dialog.dismiss();

			}
		});
		Button canceldialogButton = (Button) add_loan_dialog.findViewById(R.id.cancel_button_dialog);
		// if button is clicked, close the custom dialog
		canceldialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				add_loan_dialog.dismiss();
			}
		});
		return add_loan_dialog;
 
    }
}
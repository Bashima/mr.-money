package com.example.mr.money;

import java.util.ArrayList;

public class Lend_Model {
			public static ArrayList<Loan_Lend_Item> loan_lend_items;
			
			public static void Load_Lend_Model()
			{
				loan_lend_items= new ArrayList<Loan_Lend_Item>();
				loan_lend_items.add(new Loan_Lend_Item(1, "food", "20-11-2013", "7.30(2%)", "23$"));
				loan_lend_items.add(new Loan_Lend_Item(2, "food", "20-11-2013", "7.30(2%)", "23$"));
				loan_lend_items.add(new Loan_Lend_Item(3, "food", "20-11-2013", "7.30(2%)", "23$"));
				loan_lend_items.add(new Loan_Lend_Item(4, "food", "20-11-2013", "7.30(2%)", "23$"));
				loan_lend_items.add(new Loan_Lend_Item(5, "food", "20-11-2013", "7.30(2%)", "23$"));
				loan_lend_items.add(new Loan_Lend_Item(6, "food", "20-11-2013", "7.30(2%)", "23$"));
			}
			
			public static Loan_Lend_Item Loan_ListGetbyID(int id)
			{
				for(Loan_Lend_Item item: loan_lend_items)
				{
					if(item.Loan_LendId==id)
					{
						return item;
					}
				}
				return null;
			}

		}



package com.example.mr.money;

import java.util.ArrayList;

import android.content.Context;

public class History_List_Model {
	
	static Context c=ApplicationContextProvider.getContext();
	public static DatabaseHelper db=new DatabaseHelper(c);
	public static ArrayList<Expense> history_list_items;
	
	public static void LoadHistoryListModel()
	{
		
		history_list_items=db.getAllExpense();
		
	}
	
	public static Expense HistoryListGetbyID(int id)
	{
		for(Expense item: history_list_items)
		{
			if(item.entryId==id)
			{
				return item;
			}
		}
		return null;
	}

}

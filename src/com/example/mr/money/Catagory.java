package com.example.mr.money;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Catagory extends Activity {

	private DrawerLayout drawerLayout;
	private ListView drawable_listview;
	private ActionBarDrawerToggle actionBarDrawerToggle;
	ListView HistoryListview;
	/*private static  String[] catagories = {//CATAGORY NAME
        "food",
        "transport",
        "rent",
        "books",
        "tea" };*/
	private ArrayList catagories;
	
	private void readCatNameFile()
	{
		catagories=new ArrayList<String>();
		catagories.add("food");
		catagories.add( "transport");
		catagories.add("rent");
		catagories.add("books");
		catagories.add("tea");
		
		 System.out.println("reading reading reading");
		int k=5;
		String eol = System.getProperty("line.separator");
		  BufferedReader input = null;
		  try {
		    input = new BufferedReader(new InputStreamReader(openFileInput("categoryname.txt")));
		    String line;
		    StringBuffer buffer = new StringBuffer();
		    System.out.println("reading reading reading");
		    while ((line = input.readLine()) != null) {
		    	System.out.println("added added: "+line);
		    	catagories.add(line);
		    	buffer.append(line + eol);
		    }
		  } catch (Exception e) {
		     System.out.println("caught on: "+e.toString());
		  } finally {
		  if (input != null) {
		    try {
		    input.close();
		    } catch (IOException e) {
		    	System.out.println("caught on: "+e.toString());
		    }
		    }
		  }
		  System.out.println("reading reading reading");
		
		
	}
	
	private void createCategoryList()
	{
		readCatNameFile();
		ListView listView = (ListView)findViewById(R.id.catagory_list);
		ArrayAdapter<String> catagoryadapter =
			    new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, catagories);
		listView.setAdapter(catagoryadapter);
		
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_catagory);
		//drawer
				Model.LoadModel();
				drawable_listview=(ListView)findViewById(R.id.left_drawer);
				String[] ids=new String[Model.Menu_Items.size()];
				for(int i=0;i<ids.length;i++)
				{
					ids[i]=Integer.toString(i+1);
				}
				ItemAdapter adapter= new ItemAdapter(this, R.layout.row, ids);
				drawable_listview.setAdapter(adapter);
				drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
				actionBarDrawerToggle = new ActionBarDrawerToggle(
		                this,                  /* host Activity */
		                drawerLayout,         /* DrawerLayout object */
		                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
		                2,  /* "open drawer" description */
		                2  /* "close drawer" description */
		                );
				drawerLayout.setDrawerListener(actionBarDrawerToggle);
				if (android.os.Build.VERSION.SDK_INT >= 11)
				    getActionBar().setDisplayHomeAsUpEnabled(true);
				drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
				drawable_listview.setOnItemClickListener(new DrawerItemClickListener());

				//list
				 System.out.println("XXXXXXXXXreading reading reading");
				/*readCatNameFile();
				ListView listView = (ListView)findViewById(R.id.catagory_list);
				ArrayAdapter<String> catagoryadapter =
					    new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, catagories);
				listView.setAdapter(catagoryadapter);*/
				 createCategoryList();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.top_menu, menu);
		return true;
	}
	public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }
 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
 
         // call ActionBarDrawerToggle.onOptionsItemSelected(), if it returns true
        // then it has handled the app icon touch event
    	if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
        case R.id.add_new:
        	//OTHER E PRESS KORLE EKHAN THEKE START
        	final Dialog dialog = new Dialog(this);
        	
    		dialog.setContentView(R.layout.add_catagory_layout);
    		dialog.setTitle("Add Catagory");
    		dialog.setCancelable(true);
    		
    		Button dialogButton = (Button) dialog.findViewById(R.id.ok_button_dialog);
			// if button is clicked, close the custom dialog
			dialogButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					//GET TEXT FROM catagory 	then add it in the list
					////updating tamzeed
					String eol = System.getProperty("line.separator");
					
					EditText editTextCat = (EditText) dialog.findViewById(R.id.catagory);
				
					BufferedWriter writer = null;
					String addCat=editTextCat.getText().toString();
					  try {
						    writer = 
						      new BufferedWriter(new OutputStreamWriter(openFileOutput("categoryname.txt", Context.MODE_APPEND)));
						    writer.write(addCat + eol);
						   
						  } catch (Exception e) {
						      e.printStackTrace();
						  } finally {
						    if (writer != null) {
						    try {
						      writer.close();
						    } catch (IOException e) {
						      e.printStackTrace();
						    }
						    }
						  }
					dialog.dismiss();
					createCategoryList();
				}
			});
			Button cdialogButton = (Button) dialog.findViewById(R.id.cancel_button_dialog);
			// if button is clicked, close the custom dialog
			cdialogButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});
			dialog.show();
			//EKHANE SHESH
        case R.id.action_settings:
           // openSettings();
            return true;
        default:
            return super.onOptionsItemSelected(item);
    }
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
         actionBarDrawerToggle.syncState();
    }

    public class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        	if(position==0)
        	{Intent i = new Intent(Catagory.this, Overview.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==1)
            	{Intent i = new Intent(Catagory.this, History.class); startActivity(i);
            
            	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==2)
        	{Intent i = new Intent(Catagory.this, Catagory.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==3)
        	{Intent i = new Intent(Catagory.this, Budget.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==4)
        	{Intent i = new Intent(Catagory.this, LoanLend.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==5)
        	{Intent i = new Intent(Catagory.this, Savings.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==6)
        	{Intent intent = new Intent(Catagory.this,PrefsActivity.class);startActivity(intent);
        	
        	drawerLayout.closeDrawer(drawable_listview);}
        	
 
        }
    }

}


package com.example.mr.money;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CatagoryUsedItemAdapter extends ArrayAdapter<String> {

	private final Context context;
	private final String[] Ids;
	private final int ResourceId;
	public CatagoryUsedItemAdapter(Context context, int resource,String[] objects) {
		super(context, resource, objects);

		this.context=context;
		this.Ids=objects;
		this.ResourceId= resource;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		 LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	        View rowView = inflater.inflate(ResourceId, parent, false);
	        TextView catagoryNameView = (TextView) rowView.findViewById(R.id.catagoryNameView);
	        TextView catagoryUsedView = (TextView) rowView.findViewById(R.id.catagoryUsedView);
	        int id = Integer.parseInt(Ids[position]);
	        catagoryNameView.setText(Catagory_Used_Model.CatagoryGetbyId(id).categoryName);
	        catagoryUsedView.setText(Catagory_Used_Model.CatagoryGetbyId(id).amount.toString()+"$");
	        return rowView;
	}

}

package com.example.mr.money;

import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemAdapter extends ArrayAdapter<String> {

	private final Context context;
	private final String[] Ids;
	private final int rowResourceId;
	public ItemAdapter(Context context, int resource, String[] objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.Ids=objects;
		this.rowResourceId=resource;
	}
	
	public View getView(int position, View convertView, ViewGroup parent)
	{
		LayoutInflater inflater= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView= inflater.inflate(rowResourceId, parent, false);
		ImageView imageView=(ImageView)rowView.findViewById(R.id.imageView);
		TextView textView= (TextView)rowView.findViewById(R.id.textView);
		
		int id= Integer.parseInt(Ids[position]);
		String imageFile= Model.GetbyId(id).IconFile;
		
		textView.setText(Model.GetbyId(id).Name);
		
		InputStream ims=null;
		try
		{
			ims= context.getAssets().open(imageFile);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		Drawable d= Drawable.createFromStream(ims, null);
		
		imageView.setImageDrawable(d);
		return rowView;
	}
}

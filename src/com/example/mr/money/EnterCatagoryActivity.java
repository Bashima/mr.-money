package com.example.mr.money;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.text.format.Time;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class EnterCatagoryActivity extends Activity implements OnClickListener {

	ListView CatagoryListview;
	Context context=this;
	Category cat;
	DatabaseHelper db;
	String dateTime;
	/*private static final String[] catagories = {// CATAGORY R NAAM ANTE HBE
        "food",
        "transport",
        "rent",
        "books",
        "tea" };*/
	ListView listView;
	private ArrayList catagories;
	private void readCatNameFile()
	{
		catagories=new ArrayList<String>();
		catagories.add("food");
		catagories.add( "transport");
		catagories.add("rent");
		catagories.add("books");
		catagories.add("tea");
		
		 System.out.println("reading reading reading");
		int k=5;
		String eol = System.getProperty("line.separator");
		  BufferedReader input = null;
		  try {
		    input = new BufferedReader(new InputStreamReader(openFileInput("categoryname.txt")));
		    String line;
		    StringBuffer buffer = new StringBuffer();
		    System.out.println("reading reading reading");
		    while ((line = input.readLine()) != null) {
		    	System.out.println("added added: "+line);
		    	catagories.add(line);
		    	buffer.append(line + eol);
		    }
		  } catch (Exception e) {
		     System.out.println("caught on: "+e.toString());
		  } finally {
		  if (input != null) {
		    try {
		    input.close();
		    } catch (IOException e) {
		    	System.out.println("caught on: "+e.toString());
		    }
		    }
		  }
		  System.out.println("reading reading reading");
		
		
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.enter_catagory);
		ActionBar actionBar = getActionBar();
		actionBar.hide();
		cat=new Category();
		listView = (ListView)findViewById(R.id.catagory_list);
		db=new DatabaseHelper(getApplicationContext());
		
		/////////////
		readCatNameFile();
		System.out.println("size of cat: "+catagories.size());
		ArrayAdapter<String> catagoryadapter =
			    new ArrayAdapter<String>(this,android.R.layout.simple_list_item_single_choice, catagories);
		listView.setAdapter(catagoryadapter);
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		
		Button ok = (Button) findViewById(R.id.savebutton);
		// if button is clicked, close the custom dialog
		ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {	
				//getting current date time 
				Time today = new Time(Time.getCurrentTimezone());
				today.setToNow();
				System.out.println("date time now: "+" "+today.monthDay+today.month+" "+today.year+" "+today.format("%k:%M:%S"));
				dateTime=today.monthDay+"/"+(++today.month)+"/"+today.year+"  "+today.format("%k:%M:%S");


				//CHECK BOX ER KAJ
				
				
				int p =  listView.getCheckedItemPosition();
				System.out.println("string selected as XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX: "+p);   
				
				double amount=Double.parseDouble(Constatnts.amount);
				try
				{
			    // String s = ((TextView) listView.getChildAt(p)).getText().toString();    
					String s=(String) catagories.get(p);
			     System.out.println("string selected as XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX: "+s);     
			     cat.setCategoryName(s);
			     System.out.println("string selected as YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY: "+s);
			     db.createCategory(cat, amount, dateTime);
			     System.out.println("string selected as YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY: "+s);
				}catch(Exception ex)
				{
					System.out.println("string selected as XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX: "+ex.toString());   
				}
				Intent i = new Intent(EnterCatagoryActivity.this, Overview.class); startActivity(i);

			}
		});
		Button cancel = (Button) findViewById(R.id.cancelbutton);
		// if button is clicked, close the custom dialog
		cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {				
				Intent i = new Intent(EnterCatagoryActivity.this, Overview.class); startActivity(i);

			}
		});
		
		Button add = (Button) findViewById(R.id.add_catagory_button);
		
		// if button is clicked, close the custom dialog
		add.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {				
				showDialog(1);		
			}
		});
		
	}
	@Override
    protected Dialog onCreateDialog(int id) {
        final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.add_catagory_layout);
		dialog.setTitle("Add Catagory");
		dialog.setCancelable(true);
		
		Button dialogButton = (Button) dialog.findViewById(R.id.ok_button_dialog);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v)
			{
				//NEW CATAGORYR NAAM PORE INSERT catagory THEKE
				dialog.dismiss();
			}
		});
		Button cdialogButton = (Button) dialog.findViewById(R.id.cancel_button_dialog);
		// if button is clicked, close the custom dialog
		cdialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});       return dialog;
 
    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.enter_catagory, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

}

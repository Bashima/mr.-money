package com.example.mr.money;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class History_List_Adapter extends ArrayAdapter<String> {
	
	private final Context context;
	private final String[] Ids;
	private final int ResourceId;

	public History_List_Adapter(Context context, int resource, String[] objects) {
		super(context, resource, objects);
		this.context=context;
		this.Ids=objects;
		this.ResourceId= resource;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		 LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	        View rowView = inflater.inflate(ResourceId, parent, false);
	        TextView CatagoryNameView = (TextView) rowView.findViewById(R.id.History_List_catagoryname_view);
	        TextView AmountView = (TextView) rowView.findViewById(R.id.History_List_ammount_view);
	        TextView DateView= (TextView) rowView.findViewById(R.id.History_List_date_view);
	        TextView TimeView= (TextView) rowView.findViewById(R.id.History_List_time_view);
	        int id = Integer.parseInt(Ids[position]);
	        CatagoryNameView.setText(History_List_Model.HistoryListGetbyID(id).categoryName);
	        AmountView.setText(History_List_Model.HistoryListGetbyID(id).amount.toString());
	        DateView.setText(History_List_Model.HistoryListGetbyID(id).createdAt);
	        TimeView.setText("");
	       /* CatagoryNameView.setText("food");
	        AmountView.setText("21$");
	        DateView.setText("21-11-1991");
	        TimeView.setText("12.43");
	        */
	        return rowView;
	}

}

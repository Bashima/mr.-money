package com.example.mr.money;

import java.util.ArrayList;

public class Model {
	public static ArrayList<Menu_Item>Menu_Items;
	
	public static void LoadModel()
	{
		Menu_Items= new ArrayList<Menu_Item>();
		Menu_Items.add(new Menu_Item(1,"overview_icon.gif","Overview"));
		Menu_Items.add(new Menu_Item(2,"history.png","History"));
		Menu_Items.add(new Menu_Item(3,"bwAdminAddCategoryIcon.jpg","Category"));
		Menu_Items.add(new Menu_Item(4,"budget.jpg","Budget"));
		Menu_Items.add(new Menu_Item(5,"loan.jpg","Loan-Lend"));
		Menu_Items.add(new Menu_Item(6, "savings.png", "Savings"));
		Menu_Items.add(new Menu_Item(7,"setting.png","Setting"));
	}

	public static Menu_Item GetbyId(int id) {
		for(Menu_Item item:Menu_Items)
		{
			if(item.Id==id)
			{
				return item;
			}
		}
		return null;
	}

}

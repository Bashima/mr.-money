package com.example.mr.money;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper{
	
	
	/******************properties**************************************************/
	
	//database version
	  private static final int DATABASE_VERSION = 1;
	  
	//database name  
	  private static final String DATABASE_NAME = "mrMoney";
	  
	//database tables  
	  private static final String TABLE_CATEGORY = "categories";
	  private static final String TABLE_ENTRY = "entries";
	 
	  
	  
	//category table column names
	  private static final String CATEGORY_KEY_ID = "category_id";
	  private static final String CATEGORY_NAME = "category_name";
	  
	//entry table column names
	  private static final String ENTRY_KEY_ID="entry_id";
	  private static final String DATE_TIME="entry_datetime";
	  private static final String AMOUNT="entry_amount";
	  private static final String ENTRY_CATEGORY_ID="entry_category_id";
	  
	
	  
	  
	//category table create statement
	  private static final String CREATE_TABLE_CATEGORY = 
			  "CREATE TABLE "  + TABLE_CATEGORY + 
			  "(" + CATEGORY_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
					  CATEGORY_NAME   + " TEXT" + ")";
	  
	  
	//entry table create statement
	  private static final String CREATE_TABLE_ENTRY =
			  "CREATE TABLE " + TABLE_ENTRY + 
	            "(" + ENTRY_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
			     AMOUNT   + " DOUBLE," + 
	             DATE_TIME + " TEXT," + 
			     ENTRY_CATEGORY_ID + " INTEGER" +")";
	  
	  
	
	
/*******************methods***************************************************************/
	 public DatabaseHelper(Context context) {
		
	        super(context, DATABASE_NAME, null, DATABASE_VERSION);
	        System.out.println("--------------------crreating-----------------------");
	    }

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		 System.out.println("--------------------crreating-----------------------");
		db.execSQL(CREATE_TABLE_CATEGORY);
		db.execSQL(CREATE_TABLE_ENTRY);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENTRY);
		}
	
	
	/*********************************insert methods***************************************************/
	
	/*public long createCategory(Category category,double amount,String time) 
	{
		long catId = 0;
		boolean flag=true;
		String temp;
		String selectQuery = "SELECT  * FROM " + TABLE_CATEGORY;
		SQLiteDatabase dbR = this.getReadableDatabase();
	    Cursor c = dbR.rawQuery(selectQuery, null);
	   if (c.moveToFirst()) {
	        do {
	        	temp=c.getString(c.getColumnIndex(CATEGORY_NAME));
	        	if(temp.equals(category.categoryName))
	        	{
	        		flag=false;
	        		catId=c.getInt(c.getColumnIndex(CATEGORY_KEY_ID));
	        		break;
	        		
	        	}
	       
	        } while (c.moveToNext());
	    }
	    
	    if(flag==false)
	    {
	    	Entry entry=new Entry();
			entry.setEntryAmount(amount);
			entry.setDateTime(time);
			entry.setCatId(catId);
	    	updateEntry(entry,catId);
	    	
	    }
	    else
	    {
		
			SQLiteDatabase db=this.getWritableDatabase();
			ContentValues values=new ContentValues();
		//	values.put(CATEGORY_KEY_ID, category.getCategoryId());
			values.put(CATEGORY_NAME, category.getCategoryName());
			catId=db.insert(TABLE_CATEGORY, null, values);
			System.out.println("here catId: "+catId);
			Entry entry=new Entry();
			entry.setEntryAmount(amount);
			entry.setDateTime(time);
			entry.setCatId(catId);
			System.out.println("CatId: "+catId);
			createEntry(entry, catId);
	    }
		return catId;
	
	}*/
	
	public long createCategory(Category category,double amount,String time) 
	{
		long catId = 0;
		SQLiteDatabase db=this.getWritableDatabase();
		ContentValues values=new ContentValues();
	//	values.put(CATEGORY_KEY_ID, category.getCategoryId());
		values.put(CATEGORY_NAME, category.getCategoryName());
		catId=db.insert(TABLE_CATEGORY, null, values);
		System.out.println("here catId: "+catId);
		Entry entry=new Entry();
		entry.setEntryAmount(amount);
		entry.setDateTime(time);
		entry.setCatId(catId);
		System.out.println("CatId: "+catId);
		createEntry(entry, catId);
    
		return catId;
	

		
	}
	
	public long updateEntry(Entry entry,long catId)
	{
		boolean flag=true;
		double upAmount = 0;
		long temp,entryId = 0;
		String selectQuery = "SELECT  * FROM " + TABLE_ENTRY;
		SQLiteDatabase dbR = this.getReadableDatabase();
	    Cursor c = dbR.rawQuery(selectQuery, null);
	    if (c.moveToFirst()) {
	        do {
	        	temp=c.getInt(c.getColumnIndex(ENTRY_CATEGORY_ID));
	        	if(temp==catId)
	        	{
	        		flag=false;
	        		entryId=c.getInt(c.getColumnIndex(ENTRY_KEY_ID));
	        		upAmount=c.getDouble(c.getColumnIndex(AMOUNT));
	        		break;
	        		
	        	}
	       
	        } while (c.moveToNext());
	    }
	    upAmount+=entry.getEntryAmount();
	    
	    ContentValues values = new ContentValues();
	    values.put(AMOUNT, upAmount);
	    return dbR.update(TABLE_ENTRY, values, ENTRY_KEY_ID + " = ?",
	            new String[] { String.valueOf(entryId) });
	  
	    
	    
		
	}
	public long createEntry(Entry entry,long catId)
	{
		SQLiteDatabase db=this.getWritableDatabase();
		ContentValues values=new ContentValues();
	//	values.put(ENTRY_KEY_ID,entry.getEntryId() );
		values.put(AMOUNT, entry.getEntryAmount());
		values.put(DATE_TIME, entry.getEntryDateTime());
		
		values.put(ENTRY_CATEGORY_ID,(int)catId );
		long entryId=db.insert(TABLE_ENTRY, null, values);
		System.out.println("entryId: "+entryId);
		return entryId;
			
	}
	
/*	 public void closeDB() {
	        SQLiteDatabase db = this.getReadableDatabase();
	        if (db != null && db.isOpen())
	            db.close();
	    }
	*/
	
	/////////////////////////////////getter methods/////////////////////////////////////////////
	public Category getCategory(long id )
	{
		SQLiteDatabase db=this.getReadableDatabase();
		String selectQuery = "SELECT  * FROM " + TABLE_CATEGORY + " WHERE "
	            + CATEGORY_KEY_ID + " = " + id;
		 Cursor c = db.rawQuery(selectQuery, null);
		 if (c != null)
		        c.moveToFirst();
		 Category category=new Category();
		 category.setCategoryId(c.getInt(c.getColumnIndex(CATEGORY_KEY_ID)));
		 category.setCategoryName(c.getString(c.getColumnIndex(CATEGORY_NAME)));
		 return category;
		 		 
	}
	
	public Entry getEntry(long id)
	{
		SQLiteDatabase db=this.getReadableDatabase();
		String selectQuery = "SELECT  * FROM " + TABLE_ENTRY + " WHERE "
	            + ENTRY_KEY_ID + " = " + id;
		 Cursor c = db.rawQuery(selectQuery, null);
		 if (c != null)
		        c.moveToFirst();
		 Entry entry=new Entry();
		 entry.setEntryId(c.getInt(c.getColumnIndex(ENTRY_KEY_ID)));
		 entry.setEntryAmount(c.getDouble(c.getColumnIndex(AMOUNT)));
		 entry.setDateTime(c.getString(c.getColumnIndex(DATE_TIME)));
		 entry.setCatId(c.getInt(c.getColumnIndex(ENTRY_CATEGORY_ID)));
		 return entry;
		
		
	}
	
	public ArrayList<Expense> getAllExpense()
	{
		
		System.out.println("hello there 1st");
		ArrayList<Expense> list=new ArrayList<Expense>();
		String selectQuery = "SELECT  * FROM " + TABLE_ENTRY;
		SQLiteDatabase db = this.getReadableDatabase();
		//System.out.println("hello there 1st");
	    Cursor c = db.rawQuery(selectQuery, null);
	    System.out.println("hello there 4th");
		
	    if (c.moveToFirst()) {
	        do {
	        	System.out.println("hello hello hello");
	        	
	        	Expense expense=new Expense();
	            
	        	expense.setAmount(c.getDouble(c.getColumnIndex(AMOUNT)));
	            expense.setTime(c.getString(c.getColumnIndex(DATE_TIME)));
	            long id=c.getLong(c.getColumnIndex(ENTRY_CATEGORY_ID));
	            Category cat=getCategory(id);
	            expense.setName(cat.getCategoryName());
	            expense.setCatId(id);
	            expense.setEntryId(c.getInt(c.getColumnIndex(ENTRY_KEY_ID)));
	            
	         
	            list.add(expense);
	 
	            // adding to tags list
	           
	        } while (c.moveToNext());
	        
	      }
	    System.out.println("hello there 2nd");
		return list;
	    
	}
	
	public double getTotalExpense()
	{
		String selectQuery = "SELECT  * FROM " + TABLE_ENTRY;
		 double totalAmount=0;
		SQLiteDatabase db = this.getReadableDatabase();
		//System.out.println("hello there 1st");
	    Cursor c = db.rawQuery(selectQuery, null);
	    System.out.println("hello there 4th");
		
	    if (c.moveToFirst()) {
	        do {
	        	 System.out.println("hello there 4th amount: "+totalAmount);
	        	totalAmount+=c.getDouble(c.getColumnIndex(AMOUNT));
	        
	        	
	        } while (c.moveToNext());
	        
	      }
	    
	    System.out.println("hello there WWWWWWWWWWWWWWW amount: "+totalAmount);
	    
	    return totalAmount;
		
		
	}
	
	

}


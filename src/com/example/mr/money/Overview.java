package com.example.mr.money;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class Overview extends Activity {
	//tamzeed latest version 1.2
	///tamzeed latest version 1.0
	
	private Double budget,expenditure, remainings,amount;
	private DrawerLayout drawerLayout;
	final Context context= this;
	private ListView drawable_listview;
	String month_name,amountStr;
	public String AppName="Mr. Money";
	private ActionBarDrawerToggle actionBarDrawerToggle;
	private String filename = "myfile";
	private FileOutputStream outputStream;
	private DatabaseHelper db;
	private String strBudget;
	
	private String readBudgetFile()
	{
		String ret = "0";
        
        try {
            InputStream inputStream = openFileInput(filename);
             
            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                 
                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }
                 
                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
            catch (FileNotFoundException e) {
              System.out.println("file not found while reading");
            } catch (IOException e) {
            	
            	System.out.println("i/o exception while reading");
            }
        return ret;
		
	}
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_overview);
		
		//////testing for date time
		Time today = new Time(Time.getCurrentTimezone());
		today.setToNow();
		System.out.println("date time now: "+" "+today.monthDay+" "+(++today.month)+" "+today.year+" "+today.format("%k:%M:%S"));
		
		
		
	///setting month text	
		Calendar cal=Calendar.getInstance();
		SimpleDateFormat month_date = new SimpleDateFormat("MMMMMMMMM");
		month_name = month_date.format(cal.getTime());
		final TextView month=(TextView) findViewById(R.id.month);
		month.setText(month_name);
	
			
	
		TextView monthly_budget = (TextView) findViewById(R.id.monthly_budget); 
		TextView monthly_expenditure = (TextView) findViewById(R.id.expenditure_value);
		TextView monthly_remainings = (TextView) findViewById(R.id.remaining_value); 
		db=new DatabaseHelper(getApplicationContext());
		
		///setting budget text		
		strBudget=readBudgetFile();
		budget=Double.parseDouble(strBudget);
		Constatnts.budget=budget;
		if(budget>0)
		{
			monthly_budget.setText(strBudget+"$");
		
			///setting expense text
			amount=db.getTotalExpense();
			Constatnts.expenditure=amount;
			amountStr=amount.toString();
			monthly_expenditure.setText(amountStr+"$");
			

			///setting remaining text	
			remainings=budget-amount;
			monthly_remainings.setText(remainings+"$");	
		}
		
		else
		{
			amount=db.getTotalExpense();
			Constatnts.expenditure=amount;
			amountStr=amount.toString();
			System.out.println("testing for null: "+amountStr);
			monthly_budget.setText("not set");
			monthly_expenditure.setText(amountStr+"$");
			monthly_remainings.setText("0.00");	
			
		}
		
		
	
		
		
	
		
		
		
		final Button dButton = (Button) findViewById(R.id.button_seemore);
		dButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent j = new Intent(Overview.this, Spend_Catagory.class); 
				startActivity(j);
			}
		});
		//drawer
		Model.LoadModel();
		drawable_listview=(ListView)findViewById(R.id.left_drawer);
		String[] ids=new String[Model.Menu_Items.size()];
		for(int i=0;i<ids.length;i++)
		{
			ids[i]=Integer.toString(i+1);
		}
		ItemAdapter adapter= new ItemAdapter(this, R.layout.row, ids);
		drawable_listview.setAdapter(adapter);
		
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                0,  /* "open drawer" description */
                0  /* "close drawer" description */
                );
		drawerLayout.setDrawerListener(actionBarDrawerToggle);
		if (android.os.Build.VERSION.SDK_INT >= 11)
		    getActionBar().setDisplayHomeAsUpEnabled(true);
		drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		drawable_listview.setOnItemClickListener(new DrawerItemClickListener());
	}
	
	public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }
 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
 
         // call ActionBarDrawerToggle.onOptionsItemSelected(), if it returns true
        // then it has handled the app icon touch event
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
        	
        case R.id.add_new:
        	final Dialog dialog = new Dialog(this);
        	
        	
    		dialog.setContentView(R.layout.enter_money_layout);
    		dialog.setTitle("Expenditure");
    		dialog.setCancelable(true);
    		
    		Button dialogButton = (Button) dialog.findViewById(R.id.ok_button_dialog);
			// if button is clicked, close the custom dialog
			dialogButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					//CATAGORY R NAAM PORBA Amount THEKE
					
					EditText editTextCat = (EditText) dialog.findViewById(R.id.Amount);
					String amount = editTextCat.getText().toString();
					Constatnts.amount=amount;
					
					System.out.println("XXXXXXXXXXXXXXXXXXXXXamount is: "+Constatnts.amount);

					
					Intent i = new Intent(Overview.this, EnterCatagoryActivity.class); startActivity(i);
					dialog.dismiss();
					
					//drawable_listview.setOnItemClickListener(new DrawerItemClickListener());
				}
			});
			Button cdialogButton = (Button) dialog.findViewById(R.id.cancel_button_dialog);
			// if button is clicked, close the custom dialog
			cdialogButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});
			dialog.show();
        case R.id.action_settings:
           // openSettings();
            return true;
        default:
            return super.onOptionsItemSelected(item);
    }
        
    }
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
         actionBarDrawerToggle.syncState();
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu){
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.top_menu, menu);
	    return super.onCreateOptionsMenu(menu);
	} 

	
	public class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        	if(position==0)
        	{Intent i = new Intent(Overview.this, Overview.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==1)
            	{Intent i = new Intent(Overview.this, History.class); startActivity(i);
            
            	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==2)
        	{Intent i = new Intent(Overview.this, Catagory.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==3)
        	{Intent i = new Intent(Overview.this, Budget.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==4)
        	{Intent i = new Intent(Overview.this, LoanLend.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==5)
        	{Intent i = new Intent(Overview.this, Savings.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==6)
        	{Intent intent = new Intent(Overview.this,PrefsActivity.class);startActivity(intent);
        	
        	drawerLayout.closeDrawer(drawable_listview);}

        }
    }



}

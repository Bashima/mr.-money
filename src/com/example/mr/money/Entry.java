package com.example.mr.money;

public class Entry {
	//properties
		int entryId;
		double amount;
		String dateTime;
		long cateogryCategoryId;
		
		
		//constructors
		public Entry()
		{
			
		}
		
		public Entry(double amount,String datetime)
		{
			this.amount=amount;
			this.dateTime=datetime;
			
		}
		
		public Entry(int id,double amount,String datetime)
		{
			this.entryId=id;
			this.amount=amount;
			this.dateTime=datetime;
		}
		
		public Entry(int id,double amount,String datetime,long catid)
		{
			this.entryId=id;
			this.amount=amount;
			this.dateTime=datetime;
			this.cateogryCategoryId=catid;
		}
		
		//setters
		public void setEntryAmount(double amount)
		{
			this.amount=amount;	
		}
		
		public void setDateTime(String time)
		{
			this.dateTime=time;
		}
		
		public void setEntryId(int id)
		{
			this.entryId=id;
		}
		
		public void setCatId(long catId)
		{
			this.cateogryCategoryId=catId;
		}
		
		//getters
		public double getEntryAmount()
		{
			return this.amount;
		}
		
		public String getEntryDateTime()
		{
			return this.dateTime;
			
		}
		public int getEntryId()
		{
			return this.entryId;
			
		}
		
		public long getCatId()
		{
			return this.cateogryCategoryId;
		}


}

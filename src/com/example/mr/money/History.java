package com.example.mr.money;

import java.util.Calendar;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Field;

public class History extends Activity {
	
	private DrawerLayout drawerLayout;
	private ListView drawable_listview;
	private ActionBarDrawerToggle actionBarDrawerToggle;
	ListView HistoryListview;
	double budget_amount;
	static final int DATE_DIALOG_ID = 1;
	 private int mYear;
	 private int mMonth;
	 private int mDay;
	 private EditText etPickADate;
	@SuppressLint("NewApi")

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_history);
		budget_amount=Constatnts.expenditure;
		final TextView budget_month=(TextView) findViewById(R.id.amount_history);
		budget_month.setText(budget_amount+"S");
		System.out.println("TTTTTTTT: "+Constatnts.expenditure);
		
		//list
				History_List_Model.LoadHistoryListModel();
			        HistoryListview = (ListView) findViewById(R.id.historylistview);
			        String[] history_list_ids = new String[History_List_Model.history_list_items.size()];
			        for (int j= 0; j < history_list_ids.length; j++){

			            history_list_ids[j] = Integer.toString(j+1);
			        }

			        History_List_Adapter history_list_adapter = new History_List_Adapter(this,R.layout.history_list_layout, history_list_ids);
			        HistoryListview.setAdapter(history_list_adapter);
		//drawer
		Model.LoadModel();
		drawable_listview=(ListView)findViewById(R.id.left_drawer);
		String[] ids=new String[Model.Menu_Items.size()];
		for(int i=0;i<ids.length;i++)
		{
			ids[i]=Integer.toString(i+1);
		}
		ItemAdapter adapter= new ItemAdapter(this, R.layout.row, ids);
		drawable_listview.setAdapter(adapter);
		
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                2,  /* "open drawer" description */
                2  /* "close drawer" description */
                );
		drawerLayout.setDrawerListener(actionBarDrawerToggle);
		if (android.os.Build.VERSION.SDK_INT >= 11)
		    getActionBar().setDisplayHomeAsUpEnabled(true);
		drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		drawable_listview.setOnItemClickListener(new DrawerItemClickListener());
		
		//date picker
		
		etPickADate = (EditText) findViewById(R.id.et_datePicker);
		  etPickADate.setOnClickListener(new OnClickListener() {

		   @Override
		   public void onClick(View arg0) {
		    showDialog(DATE_DIALOG_ID);
		   }
		  });

		  final Calendar c = Calendar.getInstance();
		  mYear = c.get(Calendar.YEAR);
		  mMonth = c.get(Calendar.MONTH);
		 }

		 DatePickerDialog.OnDateSetListener mDateSetListner = new OnDateSetListener() {

		  @Override
		  public void onDateSet(DatePicker view, int year, int monthOfYear,
		    int dayOfMonth) {

		   mYear = year;
		   mMonth = monthOfYear;
		   mDay = dayOfMonth;
		   updateDate();
		  }
		 };

		 @Override
		 protected Dialog onCreateDialog(int id) {
		  switch (id) {
		  case DATE_DIALOG_ID:
		   /*
		    * return new DatePickerDialog(this, mDateSetListner, mYear, mMonth,
		    * mDay);
		    */
		   DatePickerDialog datePickerDialog = this.customDatePicker();
		   return datePickerDialog;
		  }
		  return null;
		 }

		 protected void updateDate() {
		  int localMonth = (mMonth + 1);
		  String monthString = localMonth < 10 ? "0" + localMonth : Integer
		    .toString(localMonth);
		  String localYear = Integer.toString(mYear).substring(2);
		  etPickADate.setText(new StringBuilder()
		  // Month is 0 based so add 1
		    .append(monthString).append("/").append(localYear).append(" "));
		  showDialog(DATE_DIALOG_ID);
		 }

		 private DatePickerDialog customDatePicker() {
		  DatePickerDialog dpd = new DatePickerDialog(this, mDateSetListner,
		    mYear, mMonth, mDay);
		  try {

		   Field[] datePickerDialogFields = dpd.getClass().getDeclaredFields();
		   for (Field datePickerDialogField : datePickerDialogFields) {
		    if (datePickerDialogField.getName().equals("mDatePicker")) {
		     datePickerDialogField.setAccessible(true);
		     DatePicker datePicker = (DatePicker) datePickerDialogField
		       .get(dpd);
		     Field datePickerFields[] = datePickerDialogField.getType()
		       .getDeclaredFields();
		     for (Field datePickerField : datePickerFields) {
		      if ("mDayPicker".equals(datePickerField.getName())
		        || "mDaySpinner".equals(datePickerField
		          .getName())) {
		       datePickerField.setAccessible(true);
		       Object dayPicker = new Object();
		       dayPicker = datePickerField.get(datePicker);
		       ((View) dayPicker).setVisibility(View.GONE);
		      }
		     }
		    }

		   }
		  } catch (Exception ex) {
		  }
		  return dpd;
		 }
		

	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.history, menu);
		return true;
	}
	public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }
 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
 
         // call ActionBarDrawerToggle.onOptionsItemSelected(), if it returns true
        // then it has handled the app icon touch event
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
         actionBarDrawerToggle.syncState();
    }

    public class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        	
        	if(position==0)
        	{Intent i = new Intent(History.this, Overview.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==1)
            	{Intent i = new Intent(History.this, History.class); startActivity(i);
            
            	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==2)
        	{Intent i = new Intent(History.this, Catagory.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==3)
        	{Intent i = new Intent(History.this, Budget.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==4)
        	{Intent i = new Intent(History.this, LoanLend.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==5)
        	{Intent i = new Intent(History.this, Savings.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==6)
        	{Intent intent = new Intent(History.this,PrefsActivity.class);startActivity(intent);
        	
        	drawerLayout.closeDrawer(drawable_listview);}
 
        }
    }

}

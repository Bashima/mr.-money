package com.example.mr.money;

public class Expense {
	public String categoryName;
	public Double amount;
	public String createdAt;
	public int entryId;
	public int catId;
	
	//constructors
	
	public Expense()
	{
		
	}
	
	public Expense(String catname,double amount,String createdAt,int catId,int entryId)
	{
		this.categoryName=catname;
		this.amount=amount;
		this.createdAt=createdAt;
		this.catId=catId;
		this.entryId=entryId;
		
	}
	
	//setters
	
	public void setName(String str)
	{
		this.categoryName=str;
		
	}
	public void setAmount(double amount)
	{
		this.amount=amount;
	}
	public void setTime(String time)
	{
		this.createdAt=time;
		
	}
	public void setCatId(long id)
	{
		this.catId=(int) id;
	}
	public void setEntryId(long id)
	{
		this.entryId=(int)id;
	}
	
	
	//getters
	public String getName()
	{
		return this.categoryName;
	}
	
	public double getAmount()
	{
		return this.amount;
	}
	
	public String getTime()
	{
		return this.createdAt;
	}
	public int getCatId()
	{
		return this.catId;
	}
	public int getEntrtyId()
	{
		return this.entryId;
	}



}

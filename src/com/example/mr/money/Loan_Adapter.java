package com.example.mr.money;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class Loan_Adapter extends ArrayAdapter<String> {
		
		private final Context context;
		private final String[] Ids;
		private final int ResourceId;

		public Loan_Adapter(Context context, int resource, String[] objects) {
			super(context, resource, objects);
			this.context=context;
			this.Ids=objects;
			this.ResourceId= resource;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			 LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		        View rowView = inflater.inflate(ResourceId, parent, false);
		        TextView NameView = (TextView) rowView.findViewById(R.id.from_to_name);
		        TextView AmountView = (TextView) rowView.findViewById(R.id.load_lend_amount);
		        TextView DateView= (TextView) rowView.findViewById(R.id.loan_lend_date);
		        TextView InterestView= (TextView) rowView.findViewById(R.id.loan_lend_interest);
		        int id = Integer.parseInt(Ids[position]);
		        NameView.setText(Loan_Model.Loan_ListGetbyID(id).Loan_LendName);
		        AmountView.setText(Loan_Model.Loan_ListGetbyID(id).Loan_LendAmount);
		        DateView.setText(Loan_Model.Loan_ListGetbyID(id).Loat_LendDate);
		        InterestView.setText(Loan_Model.Loan_ListGetbyID(id).Loan_LendInterest);
		        return rowView;
		}

	}


package com.example.mr.money;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class LoanLend extends TabActivity {
	Double doubleValue;
	private DrawerLayout drawerLayout;
	final Context context= this;
	private ListView drawable_listview;
	public String AppName="Mr. Money";
	private ActionBarDrawerToggle actionBarDrawerToggle;
	 
	@Override
		public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_loan_lend);
		
	        TabHost host = (TabHost)findViewById(android.R.id.tabhost);

        
	        Intent loan_intent;
	        loan_intent = new Intent().setClass(this, Loan.class);
	        TabSpec loan_spec = host.newTabSpec("Loan_Tab");
	        loan_spec.setIndicator("Loan");
	        loan_spec.setContent(loan_intent);
	        host.addTab(loan_spec);
  
	        TabSpec lend_spec = host.newTabSpec("Lend_Tab");
	        lend_spec.setIndicator("Lend");
	        Intent lend_intent = new Intent(this, Lend.class);
	        lend_spec.setContent(lend_intent);
	        host.addTab(lend_spec);
		//drawer
		Model.LoadModel();
		drawable_listview=(ListView)findViewById(R.id.left_drawer);
		String[] ids=new String[Model.Menu_Items.size()];
		for(int i=0;i<ids.length;i++)
		{
			ids[i]=Integer.toString(i+1);
		}
		ItemAdapter adapter= new ItemAdapter(this, R.layout.row, ids);
		drawable_listview.setAdapter(adapter);
		
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout,R.drawable.ic_drawer,0,0);
		drawerLayout.setDrawerListener(actionBarDrawerToggle);
		if (android.os.Build.VERSION.SDK_INT >= 11)
		    getActionBar().setDisplayHomeAsUpEnabled(true);
		drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		drawable_listview.setOnItemClickListener(new DrawerItemClickListener());
		
		
		
	}
	public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }
 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        else
        	return false;
        
    }
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
         actionBarDrawerToggle.syncState();
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu){
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.top_menu, menu);
	    return super.onCreateOptionsMenu(menu);
	} 

	
	public class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        	
        	if(position==0)
        	{Intent i = new Intent(LoanLend.this, Overview.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==1)
            	{Intent i = new Intent(LoanLend.this, History.class); startActivity(i);
            
            	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==2)
        	{Intent i = new Intent(LoanLend.this, Catagory.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	if(position==3)
        	{Intent i = new Intent(LoanLend.this, Budget.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==4)
        	{Intent i = new Intent(LoanLend.this, LoanLend.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==5)
        	{Intent i = new Intent(LoanLend.this, Savings.class); startActivity(i);
        
        	drawerLayout.closeDrawer(drawable_listview);}
        	
        	if(position==6)
        	{Intent intent = new Intent(LoanLend.this,PrefsActivity.class);startActivity(intent);
        	
        	drawerLayout.closeDrawer(drawable_listview);}
        
        }
    }



}



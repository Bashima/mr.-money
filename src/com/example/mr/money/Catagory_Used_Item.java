package com.example.mr.money;

public class Catagory_Used_Item {
	
	public int Catagory_Id;
	public String Catagory_Name;
	public Double Catagory_Used;
	
	public Catagory_Used_Item(int id,String name,Double used)
	{
		Catagory_Id=id;
		Catagory_Name=name;
		Catagory_Used=used;
	}

}

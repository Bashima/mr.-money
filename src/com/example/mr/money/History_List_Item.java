package com.example.mr.money;

public class History_List_Item {
	
	public int HistoryListId;
    public String HistoryListName;
    public String HistoryListDate;
    public String HistoryListTime;
    public String HistoryListAmount;

    public History_List_Item(int id, String name, String date, String time, String amount) {

        HistoryListId=id;
        HistoryListName= name;
        HistoryListDate= date;
        HistoryListTime= time;
        HistoryListAmount= amount;

    }

}
